package UnitTests.testsWithLibrary;

import UnitTests.PastQuiz.quiz.spaceships.CommandCenter;
import UnitTests.PastQuiz.quiz.spaceships.Spaceship;
import UnitTests.PastQuiz.quiz.spaceships.SpaceshipFleetManager;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipFleetManagerTestWithLibrary {

    ArrayList<Spaceship> spaceships = new ArrayList<>();
    SpaceshipFleetManager center = new CommandCenter();



    @AfterEach
    public void clearData() {
        spaceships.clear();
    }
    @DisplayName("Проверка на то, возвращает ли метод null, если нет военных кораблей")
    @Test
     void getMostPowerfulShip_checkNull() {
        spaceships.add(new Spaceship("foo", 0, 0,0));
        spaceships.add(new Spaceship("bar", 0,0,0));
        spaceships.add(new Spaceship("baz",0,0,0));
        Spaceship result =  center.getMostPowerfulShip(spaceships);
        Assertions.assertNull(result);
    }

    @DisplayName("Проверка на то, возвращает ли метод корабль самый хорошо вооруженный")
    @Test
     void getMostPowerfulShip_isReturnMostPowerfulShip() {
        spaceships.add(new Spaceship("foo", 45, 0,0));
        Spaceship bar = new Spaceship("bar", 10200,0,0);
        spaceships.add(bar);
        spaceships.add(new Spaceship("baz",456,0,0));
        Spaceship result = center.getMostPowerfulShip(spaceships);
        Assertions.assertEquals(bar, result);
    }

    @DisplayName("Проверка на то, что если кораблей с одинаковой мощностью несколько, возвращается первый")
    @Test
    void getMostPowerfulShip_isReturnFirst_isReturnFirst() {
        Spaceship foo = new Spaceship("foo", 1000, 0,0);
        spaceships.add(foo);
        spaceships.add(new Spaceship("bar", 1000,0,0));
        Spaceship result = center.getMostPowerfulShip(spaceships);
        Assertions.assertEquals(foo,result);
    }

    @DisplayName("Проверка на то, возвращает ли корабль по имени")
    @Test
    void getShipByName_isReturnShipByName() {
        Spaceship bar = new Spaceship("bar", 0,0,0);
        spaceships.add(bar);
        spaceships.add(new Spaceship("baz",0,0,0));
        Spaceship result =  center.getShipByName(spaceships, "bar");
        Assertions.assertEquals(bar, result);
    }

    @DisplayName("Проверка на то, что возвращает null, когда нет кораблей с заданным именем")
    @Test
    void getShipByName_isReturnNullWhenNotFound() {
        spaceships.add(new Spaceship("foo", 0, 0,0));
        spaceships.add(new Spaceship("bar", 0,0,0));
        Spaceship result = center.getShipByName(spaceships, "qzw");
        Assertions.assertNull(result);
    }

    @DisplayName("Проверка на то, возвращает ли корабли с достаточным грузовым местом")
    @Test
    void getShipsWithEnoughCargoSpace_isReturnShipsWithEnoughCargoSpace() {
        Spaceship bar = new Spaceship("bar", 0,500,0);
        Spaceship baz = new Spaceship("baz", 0,1000,0);
        spaceships.add(new Spaceship("foo", 0, 45,1));
        spaceships.add(bar);
        spaceships.add(baz);
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        shipsWithEnoughCargoSpace.add(bar);
        shipsWithEnoughCargoSpace.add(baz);
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(spaceships,500);
        Assertions.assertEquals(shipsWithEnoughCargoSpace, result);
    }

    @DisplayName("Проверка на то, возвращает ли null когда нет кораблей с  грузовым местом ")
    @Test
    void getShipsWithEnoughCargoSpace_isReturnNullWhenNoShipsWithEnoughCargoSpace() {
        spaceships.add(new Spaceship("foo", 0, 0,0));
        spaceships.add(new Spaceship("bar", 0,0,0));
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(spaceships,500);
        Assertions.assertNull(result);
    }

    @DisplayName("Проверка на то, возвращает ли мирные корабли")
    @Test
    void getAllCiviliansShips_isReturnsAllCiviliansShips() {
        Spaceship foo = new Spaceship("foo", 0, 0,0);
        Spaceship bar = new Spaceship("bar", 0, 0,0);
        ArrayList<Spaceship> check = new ArrayList<>();
        check.add(foo);
        check.add(bar);
        spaceships.add(foo);
        spaceships.add(bar);
        spaceships.add(new Spaceship("baz",456,0,0));
        ArrayList<Spaceship> result = center.getAllCivilianShips(spaceships);
        Assertions.assertEquals(check, result);
    }

    @DisplayName("Проверка на то, возвращает ли null, когда нет мирных кораблей")
    @Test
    void getAllCiviliansShips_isReturnNullWhenNoCiviliansShips() {
        spaceships.add(new Spaceship("foo",132,0,0));
        spaceships.add(new Spaceship("bar",799,0,0));
        ArrayList<Spaceship> result = center.getAllCivilianShips(spaceships);
        Assertions.assertNull(result);
    }
}
